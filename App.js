/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import { Root } from "native-base";
import { Provider } from 'react-redux';
import AppNavigator from './src/navigator/appNavigator';
// import store from './src/index';
import { PersistGate } from 'redux-persist/integration/react'
import {store, persistor} from './src/index';
console.disableYellowBox = true;
export default class App extends React.PureComponent {
    constructor(props) {
        super(props);
        
    }

    render() {
        return (
            <Provider store={store}>
                <PersistGate loading={null} persistor={persistor}>
                    <Root>
                        <AppNavigator />
                    </Root>
                </PersistGate>
            </Provider>
        );
    }
}

// build android cd android && ./gradlew assembleRelease