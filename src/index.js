// import { createStore, applyMiddleware } from 'redux';
// import  rootReducer from '../src/reducers/auth/index';
// import thunk from 'redux-thunk';
// import { persistStore, persistReducer } from 'redux-persist';
// const store = createStore(rootReducer, applyMiddleware(thunk));

// export default store;


import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist';
// import AppReducer from '../reducers/AppReducer';
import  rootReducer from '../src/reducers/auth/index';
import AsyncStorage from '@react-native-community/async-storage';

const persistConfig = {
	key: 'jinzaiapp',
	storage: AsyncStorage
}

const persistedReducer = persistReducer(persistConfig, rootReducer)
export const store = createStore(
	persistedReducer,
	applyMiddleware(thunk)
)

export const persistor = persistStore(store);

