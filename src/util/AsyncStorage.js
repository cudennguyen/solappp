import { AsyncStorage, Alert } from 'react-native';
import Configs from '../../src/configs/index';
import store from '../../src/index';
import { strings } from "../i18n";
import { connect } from 'react-redux'
const UtilAsyncStorage = {
    getStorageLanguage: () => {
        return AsyncStorage.getItem('language').then((data) => {
            const dataParse = JSON.parse(data);
            return dataParse;
        });
    },
    fetchAPI: (url, body) => {
        const status = 0;
        return fetch(url, body).then((response) => {
            if (response.status == '403') {
                this.status = 1;
                throw new Error("Login Please!")
            }
            if (response.status == '400') {
                this.status = 2;
                throw new Error("Login False")
            }
            return response.json()
        })
            .then((responseJson) => {
                return responseJson
            })
            .catch((error) => {
                this.status == 1 && Alert.alert(
                    strings("common.notLogged"),
                    strings("common.requestLogin"),
                    [{
                        text: strings("common.cancel"),
                        style: 'cancel',
                    },
                    {
                        text: strings("common.ok"),
                        onPress: () => {
                            this.props.navigation.navigate('LoginScreen');
                        }
                    }]
                )
            });
    },
    setLogin: (user) => {
        AsyncStorage.setItem('user', JSON.stringify(user));
    },
    setLogout: () => {
        var logout_data = {
            "status": false,
            "message": "Logout",
            "data": {
                "token": "",
                "type": "Bearer",
                "expires": 3600
            }
        }
        AsyncStorage.setItem('user', JSON.stringify(logout_data));
    }
}
export default UtilAsyncStorage 