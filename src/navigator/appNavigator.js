import { createAppContainer } from 'react-navigation'
import TabNavigator from './tabNavigator';
const AppNavigator = createAppContainer(TabNavigator);
export default AppNavigator;