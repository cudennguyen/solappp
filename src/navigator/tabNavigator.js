import { createBottomTabNavigator } from 'react-navigation-tabs';
import HomeNavigator from './homeNavigator';
import AccountNavigator from './accountNavigator';
const RouteTabNavigator = {
    HomeNavigator: { screen: HomeNavigator,},
    AccountNavigator: { screen: AccountNavigator}
};

const BottomTabNavigatorConfig = {
    tabBarOptions: {
        activeTintColor: "red",
        inactiveTintColor: "red",
        style: {
            backgroundColor: 'red',
        },
        tabStyle: {
            backgroundColor: "red"
        }
    },
    initialRouteName: 'HomeNavigator'
    
};

const TabNavigator = createBottomTabNavigator(RouteTabNavigator, BottomTabNavigatorConfig);
export default TabNavigator;
