import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { Platform } from 'react-native';
import HomeScreen from '../pages/Main';
import HistoryScreen from '../pages/History';
import QRCodeScreen from '../pages/QRScaner';
import SettingScreen from '../pages/Setting';
import LoginScreen from '../pages/Login';
import Icon from 'react-native-vector-icons/Ionicons'
const RouteHomeNavigator = {
    HomeScreen: {
        screen: HomeScreen,
        navigationOptions: {
            headerShown: false
        },
    },
    HistoryScreen: {
        screen: HistoryScreen,
        navigationOptions: {
            headerShown: false
        },
    },
    QRCodeScreen: {
        screen: QRCodeScreen,
        navigationOptions: {
            headerShown: false
        },
    },
    SettingScreen: {
        screen: SettingScreen,
        navigationOptions: {
            headerShown: false
        },
    },
    LoginScreen: {
        screen: LoginScreen,
        navigationOptions: {
            headerShown: false
        }
    }
};

const ConfigHomeNavigator = {
    initialRouteName: 'HomeScreen',
    
}

const HomeNavigator = createStackNavigator(RouteHomeNavigator, ConfigHomeNavigator);
HomeNavigator.navigationOptions = ({ navigation, navigationOptions }) => {
    let tabBarVisible = true;

    let routeName = navigation.state.routes[navigation.state.index].routeName

    if ( routeName == 'QRCodeScreen' ) {
        tabBarVisible = false
    }
    if ( routeName == 'LoginScreen' ) {
        tabBarVisible = false
    }

    return ({
        tabBarLabel: 'Home',
        tabBarOptions: {
            activeTintColor: '#fec105',
            // style: {
            //     backgroundColor: ColorIcon.background
            // }
        },
        tabBarVisible: tabBarVisible,
        tabBarIcon: ({focused, horizontal, tintColor}) => <Icon name={Platform.OS === 'ios' ? `ios-home` : 'md-home'} style={{color: tintColor, fontSize: 25,}}/>
    })
}

export default HomeNavigator;
