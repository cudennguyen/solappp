import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { Platform } from 'react-native';
import QRScanerScreen from '../pages/QRScaner';
import { Icon } from 'native-base'

const RouteAccountNavigator = {
    QRScanerScreen: {
        screen: QRScanerScreen,
        navigationOptions: {
            headerShown: false
        }
    }
};

const ConfigAccountNavigator = {
    initialRouteName: 'QRScanerScreen',
    headerMode: 'screen'
}

const AccountNavigator = createStackNavigator(RouteAccountNavigator, ConfigAccountNavigator);
AccountNavigator.navigationOptions = ({ navigation, navigationOptions }) => {
    return ({
        tabBarLabel: 'Check in',
        tabBarOptions: {
            activeTintColor: '#fec105',
            // style: {
            //     backgroundColor: ColorIcon.tintColor 
            // }
        },
        tabBarIcon: ({focused, horizontal, tintColor}) => <Icon name={Platform.OS === 'ios' ? `ios-location` : 'md-location'} style={{color: tintColor, fontSize: 25,}}/>
    })
}

export default AccountNavigator;
