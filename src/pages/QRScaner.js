/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react';
import { Image, StyleSheet, View, Dimensions, Alert, Modal, Text, FlatList, TouchableOpacity, SafeAreaView } from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import { Container } from 'native-base';
import { connect } from 'react-redux';
import {Icon } from 'native-base';
import Configs from '../../src/configs/index';
import UtilShowMessage from '../../src/util/ToastShow';
import Dialog, { DialogContent } from 'react-native-popup-dialog';
const heightW = Dimensions.get('window').height;
const widthW = Dimensions.get('window').width;
class QRScaner extends Component {
    constructor(props) {
        super(props);
        this.state = {
            reload: true,
            status: false,
            data: ''
        };

    }
    onHandleBack = () => {
        this.scanner.reactivate();
    }
    onSuccess(e) {
        if (this.props.user.childs[0]) {
            if (this.props.user.childs.length > 1) {
                this.setState({
                    status: true,
                    data: e.data
                })
            }
            else {
                if (this.props.user.childs[0]) {
                    this.checkin(this.props.user.childs[0].id, e.data)
                    this.setState({
                        data: e.data
                    })
                }
            }
        }
    }
    checkin(id, QRdata) {
        this.setState({
            status: false
        })
        var data = new FormData()
        data.append('child_id', id)
        data.append('qr_code', QRdata != '' ? QRdata : this.state.data)
        fetch(Configs.hostname + '/attender/checkin', { 
        method: 'POST',
        headers: {
            'Authorization': 'Bearer ' + this.props.token
        },
        body: data
        })
        .then((response) => response.json())
        .then((responseData) => {
            if (responseData.status == true) {
                UtilShowMessage.ToastShow(responseData.message);
                this.props.navigation.goBack()
            }
            else if (responseData.status == false){
                UtilShowMessage.ToastShow(responseData.message);
                setTimeout(() => {this.scanner? this.scanner.reactivate() :null}, 2000);
            }
        })
        .catch((error) => {
            UtilShowMessage.ToastShow(Configs.hostname + '/attender/checkin');
        })
        .done();
    }
    componentWillReceiveProps() {
        setTimeout(() => {this.scanner? this.scanner.reactivate() :null}, 2000);
    }
    render() {
        return (
            <SafeAreaView>
                <View style={Style.header}>
                 <View style={{marginLeft: 10}}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()?this.props.navigation.goBack():this.props.navigation.navigate('HomeScreen')}>
                            <Icon name="close" size={25} />
                        </TouchableOpacity>
                        <Text style={{color: 'black', fontSize: 25, fontWeight: 'bold' }}>Quét QR</Text>
                    </View>
                </View>
                <View style={{ alignItems: 'center' }}>
                    <QRCodeScanner
                        containerStyle={{
                            paddingBottom: 100
                        }} cameraStyle={{
                            height: Dimensions.get('window').height,
                        }}
                        style={{ flex: 1 }}
                        ref={(node) => { this.scanner = node }}
                        onRead={this.onSuccess.bind(this)}
                    />
                    <Image style={{ position: 'absolute', marginTop: Dimensions.get('window').height / 7, height: Dimensions.get('window').height / 2, width: Dimensions.get('window').height / 2 }} source={require('../../src/public/images/QR.png')} />
                </View>
                <Dialog
                    visible={this.state.status}
                    onTouchOutside={() => {
                        this.scanner.reactivate();
                        this.setState({ status: false });
                    }}
                >
                    <DialogContent>
                        <View style={{ width: 250 }}>
                            <Text>Vui lòng chọn học sinh để check in</Text>
                            <FlatList
                                style={{ marginTop: 2 }}
                                data={this.props.user? this.props.user.childs : []}
                                renderItem={({ item }) => (
                                    <View>
                                        <TouchableOpacity style={{ width: widthW, height: 25, borderBottomWidth: 1, alignItems: 'center', flexDirection: 'row' }} onPress={() => this.checkin(item.id, '')}>
                                            <View style={{ flexDirection: 'column', marginLeft: 5 }}>
                                                <View style={{ flexDirection: 'row' }}>
                                                    <Text style={{ fontSize: 12 }}>{item.first_name} {item.last_name}</Text>
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                )}
                                //Setting the number of column
                                numColumns={3}
                                keyExtractor={(item, index) => index.toString()}
                            />
                        </View>

                    </DialogContent>
                </Dialog>
            </SafeAreaView>
        );
    }
}
const Style = StyleSheet.create({
    centerText: {
        flex: 1,
        fontSize: 18,
        padding: 32,
        color: '#777',
    },
    textBold: {
        fontWeight: '500',
        color: '#000',
    },
    buttonText: {
        fontSize: 21,
        color: 'rgb(0,122,255)',
    },
    buttonTouchable: {
        paddingTop: 30
    },
    header: {
        // flexDirection: 'row',
        width: widthW,
        height: 80,
        borderBottomWidth: 1,
        borderBottomColor: '#BBBBBB',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3,
    },
});
const mapStateToProps = (state, ownProps) => {
    return {
        token: state.auth.token,
        user: state.auth.user
    };
}
export default QRScaner = connect(mapStateToProps, null)(QRScaner)