import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    TouchableOpacity, Dimensions, StatusBar, BackHandler, Alert, FlatList,
    SafeAreaView
} from 'react-native';
import { Text, Icon, Content, Container } from 'native-base';
import Configs from '../../src/configs/index';
import { connect } from 'react-redux';
import UtilAsyncStorage from '../../src/util/AsyncStorage';
import ProgressBar from 'react-native-progress/Bar';
import { strings } from '../../src/i18n';
import { NavigationEvents } from "react-navigation";
const heightW = Dimensions.get('window').height;
const widthW = Dimensions.get('window').width;

class Main extends Component {
    constructor(props) {
        super();
        this.state = {
            notifications: [],
            checkin: {
                num_attendance_dates: 0,
                total_date: 0
            },
            percent_checkin: 1
        }
        this._didFocusSubscription = props.navigation.addListener('didFocus', payload =>
            BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
        );
        this.willFocus = props.navigation.addListener('willFocus', () => {
        //     UtilAsyncStorage.fetchAPI(
        //     Configs.hostname + '/attender/notifications',
        //     {
        //         method: 'GET',
        //         headers: {
        //             'Authorization': 'Bearer ' + this.props.token
        //         }
        //     }
        // ).then((responseJson) => {
        //     if (responseJson.status == true) {
        //         this.setState({
        //             notifications: responseJson.data
        //         })
        //     }
        // });
        // UtilAsyncStorage.fetchAPI(
        //     Configs.hostname + '/attender/info ',
        //     {
        //         method: 'GET',
        //         headers: {
        //             'Authorization': 'Bearer ' + this.props.token
        //         }
        //     }
        // ).then((responseJson) => {
        //     this.setState({
        //         checkin: responseJson.data,
        //         percent_checkin: parseInt(responseJson.data[0].num_attendance_dates) / parseInt(responseJson.data[0].total_date)
        //     })
        // })
        });
    }
    componentDidMount() {
        this._willBlurSubscription = this.props.navigation.addListener('willBlur', payload =>
            BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
        );
        
        this.willFocus = this.props.navigation.addListener('willFocus', () => {
            UtilAsyncStorage.fetchAPI(
                Configs.hostname + '/attender/notifications',
                {
                    method: 'GET',
                    headers: {
                        'Authorization': 'Bearer ' + this.props.token
                    }
                }
            ).then((responseJson) => {
                if (responseJson.status == true) {
                    this.setState({
                        notifications: responseJson.data
                    })
                }
            });
            UtilAsyncStorage.fetchAPI(
                Configs.hostname + '/attender/info',
                {
                    method: 'GET',
                    headers: {
                        'Authorization': 'Bearer ' + this.props.token
                    }
                }
            ).then((responseJson) => {
                this.setState({
                    checkin: responseJson.data[0],
                    percent_checkin: parseInt(responseJson.data[0].num_attendance_dates) / parseInt(responseJson.data[0].total_date)
                })
                
            })
        });

      
    }
    onBackButtonPressAndroid = () => {
        Alert.alert(
            strings("messageError.requestExitApp1"),
            strings("messageError.requestExitApp2"), [{
                text: strings("common.cancel"),
                style: 'cancel'
            }, {
                text: strings("common.ok"),
                onPress: () => BackHandler.exitApp()
            },], {
            cancelable: false
        }
        )
        return true;
    };
    logout() {
        this.props.onlogout();
        this.props.navigation.navigate('LoginScreen')
    }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <SafeAreaView style={{
                flex: 1,
                backgroundColor: '#fff'
            }}>
                <StatusBar barStyle='dark-content'/>
                <NavigationEvents
                    onWillFocus={payload => {
                        if (!this.props.isLogin) {
                            this.props.navigation.navigate('LoginScreen')
                        }
                    }}
                />
                <View style={Style.header}>
                    <View style={{
                        height: 80,
                        justifyContent: 'center'
                    }}>
                        <Text style={{ fontSize: 23, fontWeight: 'bold' }}>Chào buổi sáng, {this.props.user? this.props.user.name : ''}!</Text>
                    </View>
                    <View style={{ 
                        flexDirection: 'row',
                        height: 50
                    }} >
                        <View style={{
                            flex: 1,
                            justifyContent: 'center'
                        }}>
                            <TouchableOpacity style={{ width: 100, height: 30, flexDirection: 'row', alignItems: 'center' }}>
                                <Icon name="ios-person" style={Style.headerButtonIcon} />
                                <Text style={Style.headerButtonText}>Học bạ</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{
                            flex: 1,
                            justifyContent: 'center'
                        }}>
                            <TouchableOpacity style={Style.headerButton}>
                                <Icon name="ios-mail" style={Style.headerButtonIcon} />
                                <Text style={Style.headerButtonText}>Tin nhắn</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{
                            flex: 2,
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'flex-end'
                        }}>
                            <TouchableOpacity onPress={() => navigate('SettingScreen')}>
                                <Icon name="settings" style={{ color: "#4D4D4D", fontSize: 17 }} />
                            </TouchableOpacity>
                        </View>
                        
                    </View>
                </View>
                <View>
                    <View style={Style.historyView}>
                        <View style={Style.historyViewContent}>
                            <Text style={{ fontSize: 13, color: '#E3D1C3' }}>Tổng số ngày đi học trong tháng</Text>
                            <Text style={{ fontSize: 25, color: '#E3D1C3' }}>{this.state.checkin.num_attendance_dates}/<Text style={{ fontSize: 15, color: '#50534E' }}>{this.state.checkin.total_date}</Text></Text>
                            <ProgressBar progress={this.state.percent_checkin} width={150} height={15} color={'#E3D1C3'} unfilledColor={'#4D4D4D'} borderWidth={0} borderRadius={10} />
                        </View>
                        <View style={Style.historyViewButton}>
                            <TouchableOpacity style={Style.historyButton} onPress={() => navigate('HistoryScreen')}>
                                <Text style={{ color: '#E3D1C3' }}>Lịch sử</Text>
                            </TouchableOpacity>
                            <View style={{ width: '65%' }}>
                                <TouchableOpacity style={Style.historyQRButton} onPress={() => navigate('QRCodeScreen')}>
                                    <Text style={{ color: '#7D7570' }}>Quét QR</Text>
                                </TouchableOpacity>
                            </View>

                        </View>
                    </View>
                    <FlatList
                        data={this.state.notifications}
                        renderItem={({ item }) => (
                            <View style={Style.infoView}>
                            <View style={Style.infoViewContent}>
                                <Text style={{ fontSize: 13, color: '#7D7570' }}>THÔNG TIN CHUNG</Text>
                                <Text style={{ fontSize: 13, fontWeight: 'bold' }}>{item.title}</Text>
                                <Text style={{ fontSize: 13 }} numberOfLines={2}>{item.content}</Text>
                            </View>
                            <View style={Style.infoViewButton}>
                                <TouchableOpacity style={Style.infoButton}>
                                    <Text style={{ color: '#7D7570' }}>Xem thêm</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        )}
                        //Setting the number of column
                        numColumns={3}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>
            </SafeAreaView>

        );
    }
}

const Style = StyleSheet.create({
    headerButton: {
        width: 100,
        height: 30,
        flexDirection: 'row',
        alignItems: 'center',
    },
    headerButtonText: {
        color: '#4D4D4D',
        fontSize: 13,
        marginLeft: 5
    },
    headerButtonIcon: {
        fontSize: 25,
        justifyContent: 'center',
        color: '#4D4D4D',
    },
    header: {
        backgroundColor: Configs.headerColor,
        width: widthW,
        shadowOffset:{  width: 0,  height: 5,  },
        shadowColor: 'black',
        shadowOpacity: 0.25,
        paddingLeft: 10,
        paddingRight: 10,
        marginBottom: 2
    },
    historyButton: {
        width: '35%',
        height: 40,
        borderWidth: 1,
        borderRadius: 4,
        borderColor: '#E3D1C3',
        justifyContent: 'center',
        alignItems: 'center'
    },
    historyView: {
        marginTop: 2,
        width: widthW,
        height: 150,
        backgroundColor: '#577A75',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.7,
        shadowRadius: 2.22,
        elevation: 3,
    },
    historyViewContent: {
        marginLeft: 15,
        marginTop: 10,
        height: 90,
    },
    historyViewButton: {
        marginLeft: 15,
        flexDirection: 'row',
        width: widthW,
    },
    historyQRButton: {
        width: '53%',
        height: 40,
        borderRadius: 4,
        backgroundColor: '#F3C963',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'flex-end',
        marginRight: 25
    },
    infoViewButton: {
        marginLeft: 15,
        flexDirection: 'row',
        width: widthW,
    },
    infoView: {
        marginTop: 5,
        width: widthW,
        height: 150,
        backgroundColor: '#E3D1C3',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3,
    },
    infoButton: {
        width: '35%',
        height: 40,
        borderWidth: 1,
        borderRadius: 4,
        borderColor: '#7D7570',
        justifyContent: 'center',
        alignItems: 'center'
    },
    infoViewContent: {
        marginLeft: 15,
        marginTop: 10,
        height: 90,
    },
})
const mapStateToProps = (state, ownProps) => {
    console.log("state---", state)
    return {
        token: state.auth.token,
        user: state.auth.user,
        isLogin: state.auth.isLogin
    };
}
const mapDispatchToProps = (dispatch) => {
    return {
        getAsyncStorageLanguage: () => {
            dispatch(ActionsLanguage.getAsyncStorage());
        },
        onlogout: () => {
            dispatch(ActionsAuthAuth.logout());
        },
    }
}
export default Main = connect(mapStateToProps, mapDispatchToProps)(Main)
