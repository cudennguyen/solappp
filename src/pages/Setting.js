import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    TouchableOpacity, Dimensions, StatusBar, BackHandler, Alert, FlatList,
    SafeAreaView, Linking
} from 'react-native';
import { Text, Icon, Picker, Container, Content } from 'native-base';
import UtilAsyncStorage from '../../src/util/AsyncStorage';
import Configs from '../../src/configs/index';
import { connect } from 'react-redux';
import { ScrollView } from 'react-native-gesture-handler';
const heightW = Dimensions.get('window').height;
const widthW = Dimensions.get('window').width;
const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];
var year = new Date().getFullYear(); 
var month = new Date().getMonth() + 1;
month = monthNames[month];
class History extends Component {
    constructor(props) {
        super();
        this.state = {
            data: [],
            selected: "key0"
        }
    }
    componentDidMount() {
        UtilAsyncStorage.fetchAPI(
            Configs.hostname + '/attender/attendances/history/' + this.props.user.childs[0].id,
            {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + this.props.token
                }
            }
        ).then((responseJson) => {
            if (responseJson.status == true) {
                this.setState({
                    data : responseJson.data.attendances
                })
            }
        })
    }
    onValueChange(value) {
        this.setState({
          selected: value
        });
      }
    logout() { 
        this.props.onlogout();
        this.props.navigation.navigate('LoginScreen')
    }
    render() {
        return (
            <SafeAreaView>
                <View style={Style.header}>
                    <View style={{marginLeft: 10}}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Icon name="close" size={25} />
                        </TouchableOpacity>
                        <Text style={{fontSize:25, fontWeight:'bold'}}>Thiết lập</Text>
                    </View>
                </View>
                
                <ScrollView contentContainerStyle={{paddingBottom: 100}}>
                <View style={{height:50, flexDirection:'row', width:widthW, alignItems:'center',paddingLeft:15}}>
                    <Text style={{ width: '70%', marginLeft:10, fontWeight:'bold', fontSize:17}}>CÁC THIẾT LẬP</Text>                    
                </View>
                <View style={{borderTopWidth:1,borderTopColor: '#BBBBBB', paddingLeft:15, height: 100}}>
                <Text style={{ width: '70%', marginLeft:10,  fontSize:15, marginTop:15  }}>Chọn ngôn ngữ</Text>
                    <Picker
                        note
                        mode="dropdown"
                        style={{ width: 120 }}
                        selectedValue={this.state.selected}
                        onValueChange={this.onValueChange.bind(this)}
                        >
                        <Picker.Item label="Tiếng Việt" value="key0" />
                        <Picker.Item label="English" value="key1" />
                        <Picker.Item label="le français" value="key2" />
                        <Picker.Item label="日本語" value="key3" />
                    </Picker>
                </View>
                <View style={{borderBottomWidth:1,borderBottomColor: '#BBBBBB', paddingLeft:15, height: 30}}>
                    <Text style={{ width: '70%', marginLeft:10,  fontSize:17, fontWeight:'bold'}}>GIỚI THIỆU</Text>
                </View>
                <View style={Style.linkView}> 
                    <TouchableOpacity style={Style.link} onPress={ ()=>{ Linking.openURL('http://lsm.edu.vn')}}><Text>Thoả thuận sử dụng</Text></TouchableOpacity>
                    <TouchableOpacity style={Style.link} onPress={ ()=>{ Linking.openURL('http://lsm.edu.vn')}}><Text>Chính sách riêng tư</Text></TouchableOpacity>
                    <TouchableOpacity style={Style.link} onPress={ ()=>{ Linking.openURL('http://lsm.edu.vn')}}><Text>Phản hồi phần mềm</Text></TouchableOpacity>
                </View>
                <View style={Style.logoutView}>
                    <TouchableOpacity style={Style.logout} onPress={()=> this.logout()}><Text>Thoát</Text></TouchableOpacity>
                </View>
                <View style={Style.contactView}>
                   <Text style={Style.contact}>GOEN SYSTEM CO.LTD</Text>
                   <Text style={Style.contact}>263 Khuat Duy Tien, Thanh Xuan, Ha Noi, Viet Nam</Text>
                   <Text style={Style.contact}>Contact: (084)912 380 747</Text>
                </View>
                </ScrollView>
            </SafeAreaView>

        );
    }
}

const Style = StyleSheet.create({
    iconClose: {
        fontSize: 35,
        marginLeft: 15,
        color:'#BBBBBB'
    },
    iconCalendar: {
        fontSize: 40,
        paddingRight: 10,
        color:'#BBBBBB'
    },
    iconHistory: {
        fontSize: 30,
        paddingLeft: 10,
        color:'#BBBBBB'
    },
    header: {
        // flexDirection: 'row',
        width: widthW,
        height: 80,
        shadowColor: "red",
        borderBottomWidth: 1,
        borderBottomColor: '#BBBBBB',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3,
    },
    headerRight: {
        flexDirection: 'row',
        width: '60%',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        paddingBottom: 10,
        
    },
    headerLeft: {
        flexDirection: 'column',
        width: '40%',
    },
    link: {
        marginTop:20
    },
    linkView: {
        marginLeft:25   
    },
    logoutView: {
        marginLeft:25,
        marginTop: 100,
        backgroundColor: '#f3c963',
        width: 150,
        height:50,
        alignItems:'center',
        borderRadius : 5,
        justifyContent: 'center',
    },
    logout: {
        color: '#4d4d4d'
    },
    contactView: {
        marginLeft:25,
        marginTop:30,
    },
    contact: {
        color: "#979797"
    }
})
const mapStateToProps = (state, ownProps) => {
    return {
        token: state.auth.token,
        user: state.auth.user
    };
}
const mapDispatchToProps = (dispatch) => {
    return {
        getAsyncStorageLanguage: () => {
            dispatch(ActionsLanguage.getAsyncStorage());
        },
        onlogout: () => {
            dispatch(ActionsAuthAuth.logout());
        },
    }
}
export default History = connect(mapStateToProps, mapDispatchToProps)(History)
