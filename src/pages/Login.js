import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    TouchableOpacity,
    BackHandler, Alert, Dimensions, StatusBar, TextInput, AsyncStorage,
    SafeAreaView
} from 'react-native';
import { Text, Icon, Container, Button, CheckBox } from 'native-base';
import Configs from '../../src/configs/index';
import UtilAsyncStorage from '../../src/util/AsyncStorage';
import ActionsLanguage from '../../src/actions/langguages/langguage';
import ActionsAuthAuth from '../../src/actions/auth/auth';
import Error from '../../src/components/Error';
import { connect } from 'react-redux';
import { strings } from '../../src/i18n';
import UtilShowMessage from '../../src/util/ToastShow';
const heightW = Dimensions.get('window').height;
const widthW = Dimensions.get('window').width;

class Login extends Component {
    _didFocusSubscription;
    _willBlurSubscription;

    constructor(props) {
        super();
        this.state = {
            username: 'attender98@littlesol.vn',
            password: 'attender98',
            placeholderUsername: 'Mã học sinh',
            placeholderPassword: 'Mật khẩu',
            placeholderUsernameColor: "#BBBBBB",
            placeholderPasswordColor: "#BBBBBB",
            showPass: true,
            statusLogin: true
        }
        this._didFocusSubscription = props.navigation.addListener('didFocus', payload =>
            BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
        );
    }

    componentDidMount() {
        this._willBlurSubscription = this.props.navigation.addListener('willBlur', payload =>
            BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
        );
        this.props.getAsyncStorageLanguage();
        AsyncStorage.getItem('user').then((data) => {
            dataParse = JSON.parse(data);
            if(typeof dataParse != 'undefined' && dataParse.status == true) {
                this.props.onLogin(dataParse);
                this.props.navigation.navigate('HomeScreen')
            }
        });
    }
    onBackButtonPressAndroid = () => {
        Alert.alert(
            strings("messageError.requestExitApp1"),
            strings("messageError.requestExitApp2"), [{
                text: strings("common.cancel"),
                style: 'cancel'
            }, {
                text: strings("common.ok"),
                onPress: () => BackHandler.exitApp()
            },], {
            cancelable: false
        }
        )
        return true;
    };

    componentWillUnmount() {
        this._didFocusSubscription && this._didFocusSubscription.remove();
        this._willBlurSubscription && this._willBlurSubscription.remove();
    }
    showPass = () => {
        this.setState({
            showPass: !this.state.showPass
        })
    }
    checkValidation() {
        let errorusername = ""
        let errorpassword = ""
        let erroruserColor = "white"
        let errorpasswordColor = "white"
        let check = true

        if (this.state.username == "") {
            check = false
            errorusername = strings("messageError.nullUsername")
            erroruserColor = "red"
        }
        if (this.state.password == "") {
            check = false
            errorpassword = strings("messageError.nullPassword")
            errorpasswordColor = "red"
        }
        this.setState({
            placeholderUsername: errorusername,
            placeholderPassword: errorpassword,
            placeholderUsernameColor: erroruserColor,
            placeholderPasswordColor: errorpasswordColor
        })
        return check

    }
    onLogin(){
        
        if (!this.checkValidation()) {
            return false
        }
        UtilAsyncStorage.fetchAPI(
            Configs.hostname + '/attender/login',
            {
                method: 'POST',
                headers:
                {   
                'Accept': 'application/json',
                'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    'email': this.state.username,
                    'password': this.state.password
                })
            }
        ).then((responseJson) => {
            if (responseJson.status == true) {
                this.props.onLogin(responseJson);
                alert('12312312');
                this.props.navigation.navigate('HomeScreen');
            }
            else {
                this.setState({
                    statusLogin: true
                })
                UtilShowMessage.ToastShow(strings('messageError.loginFalse'));
            }

        })

    }
    render() {
        const { status } = this.props;
        return (
            <SafeAreaView>
                <View style={Style.header}>
                    <Text style={{ fontSize: 25, fontWeight: 'bold', alignSelf: 'flex-end', marginLeft: 15, marginBottom: 10 }}>Goen School System</Text>
                </View>
                <View style={Style.body}>
                    <View style={[Style.View, { marginBottom: 20 }]}>
                        <TextInput placeholder={this.state.placeholderUsername == '' ? strings('user.userName') : this.state.placeholderUsername}
                            placeholderTextColor={this.state.placeholderUsernameColor}
                            placeholderTextSize={5}                     
                            style={Style.TextInput}
                            onChangeText={(username) => this.setState({ username })}
                            value={this.state.username}
                        />
                    </View>
                    <View style={Style.View}>
                        <TextInput placeholder={this.state.placeholderPassword == '' ? strings('user.password') : this.state.placeholderPassword}
                            placeholderTextColor={this.state.placeholderPasswordColor}
                            style={Style.TextInput}
                            onChangeText={(password) => this.setState({ password })}
                            secureTextEntry={this.state.showPass}
                            value={this.state.password} />
                        <TouchableOpacity
                            activeOpacity={0.7}
                            style={Style.btnEye}
                            onPress={this.showPass}>
                            <Icon name="md-eye" size={25} style={Style.iconEye} />
                        </TouchableOpacity>
                    </View>
                    <View style= {Style.savePass}><CheckBox checked={true} color={'#4D4D4D'}/><Text style={{ marginLeft:15}}>Lưu mật khẩu</Text></View>
                    <View style= {Style.forgotPass}><Text style={{ marginLeft:30, textDecorationLine:'underline'}}>Quên mật khẩu</Text></View>
                    <Button style={Style.Button} onPress={this.state.statusLogin ==true ? ()=>this.onLogin():  null} ><Text style={Style.Text}>Đăng nhập</Text></Button>
                </View>
            </SafeAreaView>
        );
    }
}

const Style = StyleSheet.create({
    header: {
        flexDirection: 'row',
        width: widthW,
        height: 70,
        shadowColor: "red",
        borderBottomWidth: 1,
        borderBottomColor: '#BBBBBB',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3,
    },
    body: {
        marginTop:10,
        // height:'100%',
        paddingLeft: 10,
        paddingRight: 10
    },
    Image: {
        height: heightW,
        width: widthW,
        flex: 1,
        backgroundColor: Configs.headerColor
    },
    btnEye: {
        position: 'absolute',
        right: 20,
        top: '12%',
    },
    iconEye: {
        color: 'rgba(0,0,0,0.2)',
    },
    savePass: {
        flexDirection:'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#BBBBBB',
        height:50,
        alignItems:'center'
    },
    forgotPass: {
        flexDirection:'row',
        marginTop: 10,
        height:50,
        alignItems:'center'
    },
    Icon: {
        position: 'absolute',
        color: 'white',
        marginTop: 0
    },
    TextInput: {
        fontSize: 20,
        borderColor: 'white',
        borderBottomWidth: 2,
        fontFamily: "Cochin",
        borderBottomColor:'#DDDDDD',
        height: 45,
        color: '#BBBBBB',
    },
    View: {
        width: '100%',
        marginTop: 10,
    },
    Text: {
        fontFamily: 'arial',
        fontSize: 18,
        color: 'black'
    },
    Button: {
        backgroundColor: '#DDDDDD',
        borderRadius: 5,
        width: '80%',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf:'center'
    }
})
const mapStateToProps = (state, ownProps) => {
    return {
        language: state.Language.language,
        status: state.Language.statusLanguage,
        isLogin: state.auth.isLogin
    };
}
const mapDispatchToProps = (dispatch) => {
    return {
        getAsyncStorageLanguage: () => {
            dispatch(ActionsLanguage.getAsyncStorage());
        },
        onLogin: (user) => {
            dispatch(ActionsAuthAuth.login(user));
        },
    }
}
export default Login = connect(mapStateToProps, mapDispatchToProps)(Login)
