import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    TouchableOpacity, Dimensions, StatusBar, BackHandler, Alert, FlatList,
    SafeAreaView,Image
} from 'react-native';
import { Text, Icon, Content, DatePicker } from 'native-base';
import UtilAsyncStorage from '../../src/util/AsyncStorage';
import Configs from '../../src/configs/index';
import { connect } from 'react-redux';
import moment from 'moment';

const heightW = Dimensions.get('window').height;
const widthW = Dimensions.get('window').width;
const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];
var year = new Date().getFullYear(); 
var month = new Date().getMonth() + 1;
month = monthNames[month];
class History extends Component {
    constructor(props) {
        super();
        this.state = {
            data: []
        }
    }
    componentDidMount() {
        if (this.props.user.childs[0].id) {
            UtilAsyncStorage.fetchAPI(
                Configs.hostname + '/attender/attendances/history/' + this.props.user.childs[0].id,
                {
                    method: 'GET',
                    headers: {
                        'Authorization': 'Bearer ' + this.props.token
                    }
                }
            ).then((responseJson) => {
                if (responseJson.status == true) {
                    this.setState({
                        data : responseJson.data.attendances
                    })
                }
            })
        }
    }
    setDate(value){
        if (this.props.user.childs) {
            var last_date = moment(value).daysInMonth();
            var from_date = value+'-01';
            var to_date = value+'-'+last_date
            UtilAsyncStorage.fetchAPI(
                Configs.hostname + '/attender/attendances/history/' + this.props.user.childs[0].id + '?from_date=' + from_date +'&to_date=' + to_date,
                {
                    method: 'GET',
                    headers: {
                        'Authorization': 'Bearer ' + this.props.token
                    }
                }
                ).then((responseJson) => {
                    if (responseJson.status == true) {
                        console.log(responseJson.data.attendances);
                        this.setState({
                            data : responseJson.data.attendances
                        })
                    }
                })
        }
    }
    render() {
        return (
            <SafeAreaView>
                <View style={Style.header}>
                    <View style={{marginLeft: 10}}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Icon name="close" size={25} />
                        </TouchableOpacity>
                    </View>
                    <View style={{
                        flexDirection: 'row',
                        marginBottom: 20,
                        marginLeft: 10,
                        marginRight: 10
                    }}>
                        <View style={{
                            flex: 1,
                            justifyContent: 'center'
                        }}>
                            <Text style={{color: 'black', fontSize: 25, fontWeight: 'bold' }}>Lịch sử</Text>
                        </View>
                        <View style={{
                            flexDirection: 'row',
                            alignItems: 'center'
                        }}>
                            <Icon name="ios-calendar" size={25}/>
                            <DatePicker
                                defaultDate={new Date()}
                                minimumDate={new Date(2018, 1)}
                                locale={"en"}
                                timeZoneOffsetInMinutes={undefined}
                                modalTransparent={false}
                                animationType={"fade"}
                                androidMode={"default"}
                                placeHolderText="Chọn tháng"
                                formatChosenDate={date => {return moment(date).format('YYYY-MM');}}
                                onDateChange={(value) => this.setDate(moment(value).format('YYYY-MM'))}
                                disabled={false}
                                />
                        </View>
                        
                    </View>
                </View>
                <View style={{height:50, flexDirection:'row', width:widthW, backgroundColor:'#DDDDDD', alignItems:'center'}}>
                    <Text style={{ width: '70%', marginLeft:10, fontWeight:'bold'}}>{month} {year}</Text>
                    <Text style={{ width: '30%', justifyContent: 'flex-end', alignItems: 'flex-end', fontSize:15, fontWeight:'bold'}}>{this.state.data.length} buổi</Text>
                </View>
                <View>
                <FlatList
                        style={{marginTop:2}}
                        data={this.state.data}
                        renderItem={({ item }) => (
                            <View>
                            <View style={{width:widthW, height:65, borderBottomWidth:1, borderTopWidth:1, alignItems:'center', flexDirection:'row'}}>
                                <Image source = {require('../images/login.png')} style={Style.iconHistory1}/>
                                <View style={{flexDirection:'column', marginLeft:5}}>
                                    <View style={{flexDirection:'row'}}>
                                        <Text style= {{fontSize:12,width:'40%'}}>{item.first_name} {item.last_name}</Text>
                                        <Text style={{marginLeft:25, fontSize:12, width:'25%'}}>{item.attending_date}</Text>
                                        <Text style={{marginLeft:25, fontSize:12, width:'15%'}}>{item.attending_time_begin}</Text>
                                    </View>
                                    <Text style= {{fontSize:12}}>{item.school_address}</Text>
                                </View>
                            </View>
                            {
                                item.attending_status == 2? 
                                (<View style={{width:widthW, height:65, borderBottomWidth:1, borderTopWidth:1, alignItems:'center', flexDirection:'row'}}>
                                <Image source = {require('../images/logout.png')} style={Style.iconHistory}/>
                                <View style={{flexDirection:'column', marginLeft:5}}>
                                    <View style={{flexDirection:'row'}}>
                                        <Text style= {{fontSize:12,width:'40%'}}>{item.first_name} {item.last_name}</Text>
                                        <Text style={{marginLeft:25, fontSize:12, width:'25%'}}>{item.attending_date}</Text>
                                        <Text style={{marginLeft:25, fontSize:12, width:'15%'}}>{item.attending_time_end}</Text>
                                    </View>
                                    <Text style= {{fontSize:12}}>{item.school_address}</Text>
                                </View>
                             </View>)
                                : null
                            }
                            </View>
                        )}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>
            </SafeAreaView>

        );
    }
}

const Style = StyleSheet.create({
    iconClose: {
        fontSize: 35,
        marginLeft: 15,
        color:'#BBBBBB'
    },
    iconCalendar: {
        fontSize: 40,
        paddingRight: 10,
        color:'#BBBBBB'
    },
    iconHistory: {
        width: 25,
        height:25,
        marginLeft: 10,
        color:'#BBBBBB'
    },
    iconHistory1: {
        width: 25,
        height:30,
        marginLeft: 10,
        color:'#BBBBBB'
    },
    header: {
        // flexDirection: 'row',
        width: widthW,
        height: 80,
        shadowColor: "red",
        borderBottomWidth: 1,
        borderBottomColor: '#BBBBBB',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3,
    },
    headerRight: {
        flexDirection: 'row',
        width: '60%',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        paddingBottom: 10,
        
    },
    headerLeft: {
        flexDirection: 'column',
        width: '40%',
    }
})
const mapStateToProps = (state, ownProps) => {
    return {
        token: state.auth.token,
        user: state.auth.user
    };
}
export default History = connect(mapStateToProps, null)(History)
