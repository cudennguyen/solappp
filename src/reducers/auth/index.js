import { combineReducers } from 'redux';
import auth from '../../../src/reducers/auth/auth';
import Language from '../../../src/reducers/langguage/langguage';
const rootReducer = combineReducers({
    auth,
    Language
});

export default ReducersAuthIndex = rootReducer;